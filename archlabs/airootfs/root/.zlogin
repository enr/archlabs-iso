clear
echo "
    ┌───────────────────────────────────────────────────────────────────────┐
    │                       Welcome to ArchLabs Linux                       │
    │───────────────────────────────────────────────────────────────────────│
    │                                                                       │
    │  Standard install:                                                    │
    │                                                                       │
    │      archlabs-installer                                               │
    │                                                                       │
    │  Run a live session:                                                  │
    │                                                                       │
    │      archlabs-installer -l [session]                                  │
    │                                                                       │
    │  Change the installation name:                                        │
    │                                                                       │
    │      DIST='MyDistro' archlabs-installer                               │
    │                                                                       │
    │  Set ROOT_PART and BOOT_PART to save time mounting:                   │
    │                                                                       │
    │      ROOT_PART='/dev/sda2' BOOT_PART='/dev/sda1' archlabs-installer   │
    │                                                                       │
    └───────────────────────────────────────────────────────────────────────┘
"
